# Portfolio Ankudinova Natalina \ Irkutsk State University FBKI 
## A first course and the first semester
### Introduction to Programming

**Viewing a repository with codes**
https://gitlab.com/Fyrelby1/homework/-/tree/main/1.1?ref_type=heads

**View all resources** #2

*The product of two numbers in **tkinter*** :star:
>![Снимок_экрана_2023-12-18_132519](/uploads/a951340e5821077043396c76e319dd8c/Снимок_экрана_2023-12-18_132519.png)

*Test in **tkinter*** :dizzy:
><div>Start Page </div>
><div>![Снимок_экрана_2023-12-18_132834](/uploads/b2bbdfca351dec054d7cdd8d7160f252/Снимок_экрана_2023-12-18_132834.png)</div>
><div>Questions</div>
><div>![Снимок_экрана_2023-12-18_132845](/uploads/c92e8e592997b39c782d75192e6cf72b/Снимок_экрана_2023-12-18_132845.png)</div>
><div>End Page</div>
><div>![Снимок_экрана_2023-12-18_133009](/uploads/9cb5750e30b3158c7994e5ff3d5db527/Снимок_экрана_2023-12-18_133009.png)</div>

*Rose Grande in **Turtle*** :rose: 
><div>![Снимок_экрана_2023-12-18_135040](/uploads/612429e71ba84f4a46c398387939a1ac/Снимок_экрана_2023-12-18_135040.png)</div>

*Rainbow Hypnosis* :rainbow:
><div>Start</div>
><div>![Снимок_экрана_2023-12-18_135455](/uploads/097fa87903a8637dbdbfbfac5da882e2/Снимок_экрана_2023-12-18_135455.png)</div>
><div>End</div>
><div>![Снимок_экрана_2023-12-18_135443](/uploads/b251b5f3306d58bcfbc29ec3cc956474/Снимок_экрана_2023-12-18_135443.png)</div>

### Network content markup languages
**Viewing a repository with codes**
https://gitlab.com/Fyrelby1/network-content-markup-languages/-/tree/main/1%20семестр?ref_type=heads

**View all resources** #4

*Flex Display in **HTML*** :alarm_clock:
>![image](/uploads/9e727bbb6f96b2dacc8148627c012af8/image.png)

*Playing cards in **HTML and CSS*** :flower_playing_cards:
>![image](/uploads/b268ccf01b8a01c3be855f630476dd56/image.png)

*Radio buttons and pictures in **HTML and CSS*** :spaghetti:
>![image](/uploads/2b89652587f02eaa404dd8d0f64add76/image.png)

*The Form in **HTML and CSS*** :clipboard:
>![image](/uploads/f6407a27a0d6185dac69af74af0ce425/image.png)

## A first course and the second semester
### Programming

**Viewing a repository with codes**
https://gitlab.com/Fyrelby1/homework/-/tree/main/1.2?ref_type=heads

**View all resources** #2

*The Shoot ’em up style game is written in **PyGame*** :dromedary_camel:
>![Снимок_экрана_2023-12-18_140850](/uploads/8cacafd6796a924e3b4748d69e47ca70/Снимок_экрана_2023-12-18_140850.png)

*Game Snake in **PyGame*** :snake:
><div>Start Menu</div>
><div>![Снимок_экрана_2023-12-18_141239](/uploads/28dcc12eca4e6dafb5a6d82589b85df8/Снимок_экрана_2023-12-18_141239.png)</div>
><div>Game</div>
><div>![Снимок_экрана_2023-12-18_141434](/uploads/bffd4915411fcd219ca23e060e0c6eed/Снимок_экрана_2023-12-18_141434.png)</div>

*Visualization of roads between points in **PyGame*** :foggy:
>![Снимок_экрана_2023-12-18_142007](/uploads/cbd052871d14afbc3272828f05c28d30/Снимок_экрана_2023-12-18_142007.png)

### Network content markup languages
**Viewing a repository with codes**
https://gitlab.com/Fyrelby1/network-content-markup-languages/-/tree/main/2%20семестр?ref_type=heads

**View all resources** #4
*Shadow in **HTML and CSS*** :clipboard:
>![image](/uploads/e55fc35fa8dc62cd45164b3bf95d67d7/image.png)

*SVG panda in **HTML*** :panda_face:
>![image](/uploads/a71f8981b468b02e6856c1f4c633542d/image.png)

*Animation in **HTML*** :alarm_clock:
><div>![Снимок_экрана_2023-12-18_170107](/uploads/71949a2fb5faffd62766a9248e7f297d/Снимок_экрана_2023-12-18_170107.png)</div>
><div>![Снимок_экрана_2023-12-18_170021](/uploads/417b3d8e11c8709896517472065be934/Снимок_экрана_2023-12-18_170021.png)</div>

*Flags of countries with a gradient in **HTML and CSS*** :it:
>![Снимок_экрана_2023-12-18_174255](/uploads/c19c5e5a3e5c05bfdbaff3824ee08136/Снимок_экрана_2023-12-18_174255.png)

*SVG smile in **HTML and CSS***
><div>![Снимок_экрана_2023-12-18_170244](/uploads/c9e1577154e2f3680b02db77df80093e/Снимок_экрана_2023-12-18_170244.png)</div>
><div>![Снимок_экрана_2023-12-18_170318](/uploads/50ae7f6dfd289b549a62280afce4d4a0/Снимок_экрана_2023-12-18_170318.png)</div>


## A second course and the third semester
### The basics of object-oriented programming (OOP)

**Viewing a repository with codes**
https://gitlab.com/Fyrelby1/homework/-/tree/main/2.3?ref_type=heads

**View all resources** #2

*A four-in-a-row game in **PyGame*** :scroll:
><div>Start Menu</div>
><div>![Снимок_экрана_2023-12-18_142924](/uploads/e637caedeebe2e8f1087c3f69a9861ef/Снимок_экрана_2023-12-18_142924.png)</div>
><div>Game</div>
><div>![Снимок_экрана_2023-12-18_142944](/uploads/971166166ed809aadc4194edf46428c0/Снимок_экрана_2023-12-18_142944.png)</div>

*Solar System in **Turtle*** :sun_with_face:
>![Снимок_экрана_2023-12-18_143059](/uploads/5954075832e6d86a558c02216fb92537/Снимок_экрана_2023-12-18_143059.png)

### Viewing a repository PROJECT OOP
https://gitlab.com/Fyrelby1/project_oop

*PROJECT OOP \ A novel game in **PyGame*** :video_game:
><div>Start Menu</div>
><div>![Снимок_экрана_2023-12-18_143955](/uploads/eb8ef2c5e402c2833748d3a79e33ae45/Снимок_экрана_2023-12-18_143955.png)</div>
><div>Dialogues and choices</div>
><div>![Снимок_экрана_2023-12-18_144035](/uploads/a75cc2613a6d2bbab97d576d0b5dede5/Снимок_экрана_2023-12-18_144035.png)</div>
><div>Three mini-games have been developed</div>
><div>![Снимок_экрана_2023-12-18_144200](/uploads/7825da358e62bf8daa1adef75c5d3549/Снимок_экрана_2023-12-18_144200.png)</div>
><div>![image](/uploads/ca336854285c6e896b0c83b152adc932/image.png)</div>
><div>![image](/uploads/9585a34ca1afa51b02683d83f29dcfab/image.png)</div>
><div>There are 3 endings</div>

### Web development technologies: client-side programming
**Viewing a repository with codes**
https://gitlab.com/Fyrelby1/web-development-technologie

**View all resources** #5

*Business card with **javascript***
>![image](/uploads/57b579d46c03bb1b41237cab3469f7fc/image.png)

*Puzzles with **javascript***
>![image](/uploads/9dbcf1aa2c740ba2aea2d3a105d34a06/image.png)

*Task List with **javascript***
>![image](/uploads/b7a256d315a11db7bb4ac85dd7593ec2/image.png)

## A Third  course and the fifth semester
### Design of graphical user interfaces

**Viewing a repository with codes**
https://gitlab.com/Fyrelby1/dogui

**View all resources** #3

*Password in **QML***  :scroll:
>![Снимок_экрана_2023-12-18_161004](/uploads/ad77888239393607bfd8420acf1e92c1/Снимок_экрана_2023-12-18_161004.png)

*Login Page in **QML***  :page_with_curl:
>![image](/uploads/3da53934d89399a5163c393a445619af/image.png)

*The Messenger app in **QML***  :page_facing_up:
>![image](/uploads/85e46e773ec400b19bbddf70ca334b32/image.png)

*Traffic lights using State and Transition in **QML***  :vertical_traffic_light:
>![image](/uploads/b5da562e2e53595a6ea2f2cf8cde09b1/image.png)

*Using various anchor binding options, we got an image of a duck in **QML***  :hatched_chick:
>![image](/uploads/54d1e25bfd332a6fc9c2f6261d8d5584/image.png)

*StackView VERTICAL in **QML*** :boom:
>![image](/uploads/2419411497b0cd9ebacb7e10764267c3/image.png)

*Using layouts, I made a column linker that contains a pair of row linkers and a place for the content in **QML*** :boom:
>![image](/uploads/dff4e96474b1dd1cd08bb47e87b9da65/image.png)

### Viewing a repository PROJECT QML
https://gitlab.com/Fyrelby1/project-qml/-/tree/main?ref_type=heads

*PROJECT QML \ Toy Store in **QML*** :sparkles:
><div>Pages</div>
><div>![Снимок_экрана_2023-12-18_163758](/uploads/7cd4105d333f62db68fb3ab1a6d04afc/Снимок_экрана_2023-12-18_163758.png)</div>
><div>![Снимок_экрана_2023-12-18_163804](/uploads/8d18c159f8566a65ea0fe53d2fd4851c/Снимок_экрана_2023-12-18_163804.png)</div>
><div>![Снимок_экрана_2023-12-18_163835](/uploads/d851215e2c9be3a9e709d6df4f7be5aa/Снимок_экрана_2023-12-18_163835.png)</div>
><div>![Снимок_экрана_2023-12-18_163849](/uploads/3f373377dc832028bb339337cd5e245d/Снимок_экрана_2023-12-18_163849.png)</div>


